package main

import (
	"log"
	"net/http"
)

func main() {
	// initialize new ServeMux, an HTTP request router or dispatcher
	mux := http.NewServeMux()
	// register the home function as a handler for the "/" URL pattern
	mux.HandleFunc("/", home)
	mux.HandleFunc("/snippet/view", snippetView)
	mux.HandleFunc("/snippet/create", snippetCreate)

	// Use the http.ListenAndServe() function to start a new web server.
	log.Println("Starting server on :8080")
	err := http.ListenAndServe(":8080", mux)
	log.Fatal(err)
}
